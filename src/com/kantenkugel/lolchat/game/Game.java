package com.kantenkugel.lolchat.game;

import com.kantenkugel.lolchat.statics.Config;
import lol4j.protocol.dto.game.GameDto;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Kantenkugel.
 */
class Game {
    private final int version = Config.GAMEVERSION;

    private  GlobalData global;
    private final List<Summoner> team1;
    private final List<Summoner> team2;

    private final TeamSummary teamSummary1;
    private final TeamSummary teamSummary2;

    public Game() {
        team1 = new LinkedList<>();
        team2 = new LinkedList<>();
        teamSummary1 = new TeamSummary();
        teamSummary2 = new TeamSummary();
    }

    public void addSummoner(long summId, GameDto gameDto) {
        if(global == null) {
            global = new GlobalData(gameDto);
        }
        switch(gameDto.getTeamId()) {
            case 100:
                team1.add(new Summoner(summId, gameDto));
                teamSummary1.addStats(gameDto.getStats());
                break;
            case 200:
                team2.add(new Summoner(summId, gameDto));
                teamSummary2.addStats(gameDto.getStats());
                break;
            default:
        }
    }

    public GlobalData getGlobal() {
        return global;
    }

    public List<Summoner> getTeam1() {
        return team1;
    }

    public List<Summoner> getTeam2() {
        return team2;
    }

    public TeamSummary getTeamSummary1() {
        return teamSummary1;
    }

    public TeamSummary getTeamSummary2() {
        return teamSummary2;
    }
}
