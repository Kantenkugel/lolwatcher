package com.kantenkugel.lolchat.game;

import com.github.theholywaffle.lolchatapi.LolStatus;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kantenkugel.lolchat.statics.Config;
import com.kantenkugel.lolchat.statics.SummonerHelper;
import com.kantenkugel.lolchat.util.FileHelper;
import com.kantenkugel.lolchat.util.SimpleLog;
import lol4j.client.impl.Lol4JClientImpl;
import lol4j.protocol.dto.game.GameDto;
import lol4j.protocol.dto.game.PlayerDto;

import java.io.File;
import java.util.*;


public class GameController {

    private static final SimpleLog LOG = SimpleLog.getLog("Game");

    private final Map<String, Long> active;
    private final Set<Long> activeMatches;
    private final Map<Long, Set<Long>> watchlist;
    private final Lol4JClientImpl riot;
    private final FileHelper fileHelper;

    public GameController(Lol4JClientImpl riot, File parentDir) {
        active = new HashMap<>();
        activeMatches = new HashSet<>();
        watchlist = new HashMap<>();
        new Thread(new Cleaner<>(active)).start();
        this.riot = riot;
        this.fileHelper = new FileHelper(parentDir, "games");
    }

    public void statusUpdate(String name, LolStatus.GameStatus mode) {
        synchronized (active) {
            if(mode == LolStatus.GameStatus.OUT_OF_GAME && active.containsKey(name.toLowerCase())) {
                LOG.debug("Status of " + name + " changed to out of game");
                waitForMatch(name);
                active.remove(name.toLowerCase());
            } else if(mode == LolStatus.GameStatus.IN_GAME) {
                LOG.debug("Status of " + name + " changed to ingame");
                active.put(name.toLowerCase(), System.currentTimeMillis());
            }
        }
    }

    private void waitForMatch(String summonerName) {
        long summId = SummonerHelper.getId(summonerName);
        new Thread(new HistoryRetriever(summId, System.currentTimeMillis())).start();

    }

    private void generateMatchStats(long summId, GameDto gameDto) {
        synchronized (activeMatches) {
            if(!activeMatches.contains(gameDto.getGameId())) {
                activeMatches.add(gameDto.getGameId());
                watchlist.put(gameDto.getGameId(), new HashSet<Long>());
                watchlist.get(gameDto.getGameId()).add(summId);
                LOG.info("Generating match with id " + gameDto.getGameId() + " started by " + SummonerHelper.getName(summId));
            } else {
                LOG.debug("Adding Summoner " + SummonerHelper.getName(summId) + " to watchlist of game " + gameDto.getGameId());
                watchlist.get(gameDto.getGameId()).add(summId);
                return;
            }
        }
        Game game = new Game();
        game.addSummoner(summId, gameDto);
        Set<Long> otherSummoners = otherSummoners(gameDto);
        new Thread(new HistoryRetriever(otherSummoners, game)).run();
    }

    private Set<Long> otherSummoners(GameDto main) {
        LOG.trace("Getting fellow-players' ids");
        LOG.trace("Main summoner is in team: " + main.getTeamId());
        StringBuilder b = new StringBuilder("Other summoners are: ");
        Set<Long> ids = new HashSet<>();
        for(PlayerDto others : main.getFellowPlayers()) {
            ids.add(others.getSummonerId());
            b.append(others.getTeamId()).append(":").append(SummonerHelper.getName(others.getSummonerId()));
            b.append(" (").append(others.getSummonerId()).append("), ");
        }
        b.setLength(b.length()-2);
        LOG.trace(b.toString());
        return ids;
    }

    private void saveGameSummary(Game game) {
        String filename = game.getGlobal().getId() + ".txt";
        LOG.info("Generating match-file for match " + game.getGlobal().getId());
        if(!fileHelper.exists(filename)) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            fileHelper.saveFile(filename, gson.toJson(game));
        }
        synchronized (activeMatches) {
            if(activeMatches.contains(game.getGlobal().getId())) {
                activeMatches.remove(game.getGlobal().getId());
                for(Long watchingSummoners : watchlist.get(game.getGlobal().getId())) {
                    LOG.debug("Would notify summoner "+SummonerHelper.getName(watchingSummoners));
                }
                watchlist.remove(game.getGlobal().getId());
            }
        }
    }

    private class HistoryRetriever implements Runnable {
        private final boolean single;
        private final Set<Long> ids;
        private long matchtime;
        private Game game;

        public HistoryRetriever(Set<Long> ids, Game game) {
            this.ids = ids;
            this.game = game;
            this.single = false;
            LOG.trace("Started History-retriever in multi-mode");
        }

        public HistoryRetriever(long id, long matchtime) {
            this.ids = new HashSet<>();
            this.ids.add(id);
            this.matchtime = matchtime;
            this.single = true;
            LOG.trace("Started History-retriever in single-mode");
        }

        @Override
        public void run() {
            Map<Long, GameDto> matches = new HashMap<>();
            long waited = 0;
            for(Long summId : ids) {
                LOG.info("Waiting for match history of summoner " + SummonerHelper.getName(summId) + " with id " + summId + " to update");
                boolean found = false;
                for(int i = 0; i < 60; i++) {
                    List<GameDto> gamesDto = riot.getRecentGames(Config.REGION, summId).getGames();
                    if(gamesDto.size() > 0) {
                        if(single) {
                            long riotTime = gamesDto.get(0).getCreateDate();
                            if(Math.abs(riotTime - matchtime) / 1000 < 120) {
                                generateMatchStats(summId, gamesDto.get(0));
                                return;
                            }
                        } else {
                            for(GameDto aGamesDto : gamesDto) {
                                if(Long.compare(aGamesDto.getGameId(), game.getGlobal().getId()) == 0) {
                                    matches.put(summId, aGamesDto);
                                    found = true;
                                    break;
                                } else if(Math.abs(aGamesDto.getCreateDate() - game.getGlobal().getEndTime()) / 1000 < 120) {
                                    LOG.warn("Found game with different id for summoner " + SummonerHelper.getName(summId) + "(" + summId + "... " + aGamesDto.getGameId());
                                    matches.put(summId, aGamesDto);
                                    found = true;
                                    break;
                                }
                            }
                            if(found) {
                                break;
                            }
                        }
                    } else {
                        LOG.warn("Recent match-data had size of 0... retrying soon");
                    }
                    if(waited >= 90 * 60) {
                        LOG.warn("Waited to long overall... aborting current summoner");
                        break;
                    }
                    try {
                        Thread.sleep(1000 * 30);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    waited += 30;
                    LOG.debug("Waited for overall " + waited + " secs now");
                }
                if(!found) {
                    LOG.warn("Could not retrieve game for summoner " + SummonerHelper.getName(summId) + " with id " + summId);
                    /*
                    if(!single) {
                        activeMatches.remove(game.getGlobal().getId());
                    }
                    return;
                    */
                }
            }
            if(!single) {
                for(Long summid : matches.keySet()) {
                    game.addSummoner(summid, matches.get(summid));
                }
                saveGameSummary(game);
            }
        }
    }

    private class Cleaner<T> implements Runnable {
        private final Map<T, Long> mapToWatch;

        public Cleaner(final Map<T, Long> map) {
            mapToWatch = map;
        }

        @Override
        public void run() {
            while(true) {
                try {
                    Thread.sleep(Config.CLEANER_SLEEP * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
                synchronized (mapToWatch) {
                    Map<T, Long> newMap = new HashMap<>();
                    for(T key : mapToWatch.keySet()) {
                        if(mapToWatch.get(key) + 2 * 60 * 60 * 1000 > System.currentTimeMillis()) {
                            newMap.put(key, mapToWatch.get(key));
                        } else {
                            LOG.warn(String.valueOf(key) + " was in the game for to long (>2h) so i removed him from the tracking list");
                        }
                    }
                    mapToWatch.clear();
                    mapToWatch.putAll(newMap);
                }
            }
        }
    }
}
