package com.kantenkugel.lolchat.game;

import com.kantenkugel.lolchat.statics.SummonerHelper;
import lol4j.protocol.dto.game.GameDto;
import lol4j.protocol.dto.game.RawStatsDto;

/**
 * Created by Kantenkugel.
 */
class Summoner {
    private final long summonerId;
    private final String summonerName;
    private final int level;

    private final int championId;

    private final int spellId1, spellId2;

    private final int ipEarned;

    private final RawStatsDto stats;

    public Summoner(long summId, GameDto game) {
        this.summonerId = summId;
        this.summonerName = SummonerHelper.getName(summId);
        this.level = game.getLevel();

        this.championId = game.getChampionId();

        this.spellId1 = game.getSpell1();
        this.spellId2 = game.getSpell2();

        this.ipEarned = game.getIpEarned();

        this.stats = game.getStats();
    }

    public long getSummonerId() {
        return summonerId;
    }

    public String getSummonerName() {
        return summonerName;
    }

    public int getLevel() {
        return level;
    }

    public int getChampionId() {
        return championId;
    }

    public int getSpellId1() {
        return spellId1;
    }

    public int getSpellId2() {
        return spellId2;
    }

    public int getIpEarned() {
        return ipEarned;
    }

    public RawStatsDto getStats() {
        return stats;
    }
}
