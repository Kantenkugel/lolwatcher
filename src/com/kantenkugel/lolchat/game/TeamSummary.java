package com.kantenkugel.lolchat.game;

import lol4j.protocol.dto.game.RawStatsDto;

/**
 * Created by Kantenkugel.
 */
class TeamSummary {
    private int kills = 0;
    private int deaths = 0;
    private int assists = 0;

    private int barracksKilled = 0;
    private int turretsKilled = 0;

    private int goldEarned = 0;
    private int goldSpent = 0;

    private int wardsPlaced = 0;
    private int wardsDestroyed = 0;
    private int sightWarsBought = 0;

    private int damageDealtTrue = 0;
    private int damageDealtTrueChampion = 0;
    private int damageDealtPhysical = 0;
    private int damageDealtPhysicalChampion = 0;
    private int damageDealtMagical = 0;
    private int damageDealtMagicalChampion = 0;
    private int damageDealtTotal = 0;
    private int damageDealtTotalChampion = 0;

    private int damageTakenTrue = 0;
    private int damageTakenPhysical = 0;
    private int damageTakenMagical = 0;
    private int damageTakenTotal = 0;

    private int healingDone = 0;

    private int neutralMinionsOwn = 0;
    private int neutralMinionsEnemy = 0;
    private int minionsKilled = 0;

    public TeamSummary() {}

    public void addStats(RawStatsDto statsDto) {
        kills += statsDto.getChampionsKilled();
        deaths += statsDto.getNumDeaths();
        assists += statsDto.getAssists();

        barracksKilled += statsDto.getBarracksKilled();
        turretsKilled += statsDto.getTurretsKilled();

        goldEarned += statsDto.getGoldEarned();
        goldSpent += statsDto.getGoldSpent();

        wardsPlaced += statsDto.getWardPlaced();
        wardsDestroyed += statsDto.getWardKilled();
        sightWarsBought += statsDto.getSightWardsBought();

        damageDealtMagical += statsDto.getMagicDamageDealtPlayer();
        damageDealtMagicalChampion += statsDto.getMagicDamageDealtToChampions();
        damageDealtPhysical += statsDto.getPhysicalDamageDealtPlayer();
        damageDealtPhysicalChampion += statsDto.getPhysicalDamageDealtToChampions();
        damageDealtTrue += statsDto.getTrueDamageDealtPlayer();
        damageDealtTrueChampion += statsDto.getTrueDamageDealtToChampions();
        damageDealtTotal += statsDto.getTotalDamageDealt();
        damageDealtTotalChampion += statsDto.getTotalDamageDealtToChampions();

        damageTakenMagical += statsDto.getMagicDamageTaken();
        damageTakenPhysical += statsDto.getPhysicalDamageTaken();
        damageTakenTrue += statsDto.getTrueDamageTaken();
        damageTakenTotal += statsDto.getTotalDamageTaken();

        healingDone += statsDto.getTotalHeal();

        neutralMinionsEnemy += statsDto.getNeutralMonstersKilledEnemyJungle();
        neutralMinionsOwn += statsDto.getNeutralMonstersKilledYourJungle();
        minionsKilled += statsDto.getMinionsKilled();
    }

    public int getKills() {
        return kills;
    }

    public int getDeaths() {
        return deaths;
    }

    public int getAssists() {
        return assists;
    }

    public int getBarracksKilled() {
        return barracksKilled;
    }

    public int getTurretsKilled() {
        return turretsKilled;
    }

    public int getGoldEarned() {
        return goldEarned;
    }

    public int getGoldSpent() {
        return goldSpent;
    }

    public int getWardsPlaced() {
        return wardsPlaced;
    }

    public int getWardsDestroyed() {
        return wardsDestroyed;
    }

    public int getSightWarsBought() {
        return sightWarsBought;
    }

    public int getDamageDealtTrue() {
        return damageDealtTrue;
    }

    public int getDamageDealtTrueChampion() {
        return damageDealtTrueChampion;
    }

    public int getDamageDealtPhysical() {
        return damageDealtPhysical;
    }

    public int getDamageDealtPhysicalChampion() {
        return damageDealtPhysicalChampion;
    }

    public int getDamageDealtMagical() {
        return damageDealtMagical;
    }

    public int getDamageDealtMagicalChampion() {
        return damageDealtMagicalChampion;
    }

    public int getDamageDealtTotal() {
        return damageDealtTotal;
    }

    public int getDamageDealtTotalChampion() {
        return damageDealtTotalChampion;
    }

    public int getDamageTakenTrue() {
        return damageTakenTrue;
    }

    public int getDamageTakenPhysical() {
        return damageTakenPhysical;
    }

    public int getDamageTakenMagical() {
        return damageTakenMagical;
    }

    public int getDamageTakenTotal() {
        return damageTakenTotal;
    }

    public int getHealingDone() {
        return healingDone;
    }

    public int getNeutralMinionsOwn() {
        return neutralMinionsOwn;
    }

    public int getNeutralMinionsEnemy() {
        return neutralMinionsEnemy;
    }

    public int getMinionsKilled() {
        return minionsKilled;
    }
}
