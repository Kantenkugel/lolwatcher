package com.kantenkugel.lolchat.game;

import lol4j.protocol.dto.game.GameDto;
import lol4j.util.game.GameMap;
import lol4j.util.game.GameMode;
import lol4j.util.game.GameType;
import lol4j.util.game.SubType;

/**
 * Created by Kantenkugel.
 */
class GlobalData {
    private final long id;
    private final GameType gameType;
    private final GameMode gameMode;
    private final GameMap map;
    private final SubType subType;
    private final long endTime;
    private final int duration;
    private final int winner;

    public GlobalData(GameDto game) {
        id = game.getGameId();
        gameType = game.getGameType();
        gameMode = game.getGameMode();
        map = game.getGameMap();
        subType = game.getSubType();
        endTime = game.getCreateDate();
        duration = game.getStats().getTimePlayed();

        switch(game.getTeamId()) {
            case 100:
                if(game.getStats().isWin()) {
                    winner = 1;
                } else {
                    winner = 2;
                }
                break;
            case 200:
                if(game.getStats().isWin()) {
                    winner = 2;
                } else {
                    winner = 1;
                }
                break;
            default:
                winner = 0;
        }
    }

    public long getId() {
        return id;
    }

    public GameType getGameType() {
        return gameType;
    }

    public GameMode getGameMode() {
        return gameMode;
    }

    public GameMap getMap() {
        return map;
    }

    public SubType getSubType() {
        return subType;
    }

    public long getEndTime() {
        return endTime;
    }

    public int getDuration() {
        return duration;
    }

    public int getWinner() {
        return winner;
    }
}
