package com.kantenkugel.lolchat.config;

import com.kantenkugel.lolchat.statics.Config;
import com.kantenkugel.lolchat.util.SimpleLog;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.PropertiesConfigurationLayout;
import org.apache.commons.configuration.event.ConfigurationEvent;
import org.apache.commons.configuration.event.ConfigurationListener;

import java.io.File;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Configuration extends PropertiesConfiguration implements IConfiguration, ConfigurationListener {

    private static Pattern versionPattern = Pattern.compile(".*\\#config.version\\=(\\d*)\\;.*");

    private boolean hasChanged = false;
    private boolean isNew = false;

    private int versionChanged = -1;

    private PropertiesConfigurationLayout layout;

    public Configuration(File file, String comment) throws ConfigurationException {
        super(file);
        layout = getLayout();
        super.addConfigurationListener(this);

        String version = "#config.version=" + Config.CONFIGVERSION + ";";
        String currentHeader = layout.getHeaderComment();
        String newHeader = version;

        if(comment != null) {
            newHeader = comment+" "+newHeader;
        }

        if(this.isEmpty()) {
            isNew = true;
        } else if(!currentHeader.contains(version)) {
            Matcher matcher = versionPattern.matcher(currentHeader);
            if(matcher.matches()) {
                versionChanged = Integer.parseInt(matcher.group(1));
            } else {
                versionChanged = 0;
            }
            hasChanged = true;
        }

        layout.setHeaderComment(newHeader);
    }

    @Override
    public <T extends Enum<T>> T getEnum(String key, T defaultValue, String comment) {
        key = "E_"+key;
        T out;
        String stringVal = getString(key);
        if(stringVal == null) {
            out = defaultValue;
            setProperty(key, out.toString());
        } else {
            try {
                out = T.valueOf(defaultValue.getDeclaringClass(), stringVal);
            } catch (IllegalArgumentException ex) {
                out = defaultValue;
                setProperty(key, out.toString());
                SimpleLog.getLog("Config").warn("Error reading Enum value " + key.substring(2) + " out of configuration-file... reset to default");
            }
        }
        String header = Arrays.toString(defaultValue.getDeclaringClass().getEnumConstants());
        if(comment != null) {
            header = comment+" "+header;
        }
        layout.setComment(key, header);
        layout.setBlancLinesBefore(key, 1);

        return out;
    }

    @Override
    public <T extends Enum<T>> void setEnum(String key, T value, String comment) {
        key = "E_" + key;
        setProperty(key, value.toString());
        String header = Arrays.toString(value.getDeclaringClass().getEnumConstants());
        if(comment != null) {
            header = comment + " " + header;
        }
        layout.setComment(key, header);
        layout.setBlancLinesBefore(key, 1);
    }

    @Override
    public <T extends Enum<T>> void deleteEnum(String key) {
        clearProperty("E_" + key);
    }

    @Override
    public boolean getBoolean(String key, boolean defaultValue, String comment) {
        key = "B_"+key;
        boolean out;
        try {
            out = getBoolean(key);
        } catch (NoSuchElementException ex) {
            out = defaultValue;
            super.setProperty(key, out);
        }
        if(comment != null) {
            layout.setComment(key, comment);
            layout.setBlancLinesBefore(key, 1);
        }
        return out;
    }

    @Override
    public void setBoolean(String key, boolean value, String comment) {
        key = "B_"+key;
        setProperty(key, value);
        if(comment != null) {
            layout.setComment(key, comment);
            layout.setBlancLinesBefore(key, 1);
        }
    }

    @Override
    public void deleteBoolean(String key) {
        clearProperty("B_" + key);
    }

    @Override
    public int getInteger(String key, int defaultValue, String comment) {
        key = "I_"+key;
        int out;
        try {
            out = getInt(key);
        } catch (NoSuchElementException ex) {
            out = defaultValue;
            super.setProperty(key, out);
        }
        if(comment != null) {
            layout.setComment(key, comment);
            layout.setBlancLinesBefore(key, 1);
        }
        return out;
    }

    @Override
    public void setInteger(String key, int value, String comment) {
        key = "I_" + key;
        setProperty(key, value);
        if(comment != null) {
            layout.setComment(key, comment);
            layout.setBlancLinesBefore(key, 1);
        }
    }

    @Override
    public void deleteInteger(String key) {
        clearProperty("I_" + key);
    }

    @Override
    public String getString(String key, String defaultValue, String comment) {
        key = "S_"+key;
        String out = getString(key);
        if(out == null) {
            out = defaultValue;
            setProperty(key, out);
        }
        if(comment != null) {
            layout.setComment(key, comment);
            layout.setBlancLinesBefore(key, 1);
        }
        return out;
    }

    @Override
    public void setString(String key, String value, String comment) {
        key = "S_" + key;
        setProperty(key, value);
        if(comment != null) {
            layout.setComment(key, comment);
            layout.setBlancLinesBefore(key, 1);
        }
    }

    @Override
    public void deleteString(String key) {
        clearProperty("S_" + key);
    }

    @Override
    public void configurationChanged(ConfigurationEvent event) {
        hasChanged = true;
    }

    @Override
    public boolean hasChanged() {
        return hasChanged;
    }

    @Override
    public boolean isNew() {
        return isNew;
    }

    @Override
    public boolean hasVersionChanged() {
        return versionChanged >= 0;
    }

    @Override
    public int getOldVersion() {
        return versionChanged;
    }
}