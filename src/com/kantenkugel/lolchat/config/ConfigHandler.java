package com.kantenkugel.lolchat.config;

import com.github.theholywaffle.lolchatapi.ChatMode;
import com.github.theholywaffle.lolchatapi.ChatServer;
import com.github.theholywaffle.lolchatapi.LolStatus;
import com.kantenkugel.lolchat.statics.Config;
import com.kantenkugel.lolchat.util.SimpleLog;
import lol4j.util.Region;
import org.apache.commons.configuration.ConfigurationException;

import java.io.File;

/**
 * Created by Kantenkugel.
 */
public class ConfigHandler {
    private static IConfiguration config;
    private static SimpleLog log = SimpleLog.getLog("Config");

    public static void init() {
        if(config == null) {
            try {
                config = new Configuration(new File(Config.CONFIG_PATH), Config.CONFIG_COMMENT);
                if(config.hasVersionChanged()) {
                    handleVersionChange(config.getOldVersion());
                }
                loadConfig();
                if(config.isNew()) {
                    log.info("A new configuration-file has been created at " + new File(Config.CONFIG_PATH).getAbsolutePath());
                    log.info("Please fill in all neccessary data and restart this application");
                    System.exit(0);
                }
                if(config.hasVersionChanged()) {
                    log.info("The structure of the configuration file has changed. This application will still run but you may want to change some new things");
                } else {
                    log.info("Successfully loaded Config file " + new File(Config.CONFIG_PATH).getAbsolutePath());
                }
            } catch (ConfigurationException e) {
                log.warn("Error creating/loading config-file");
                e.printStackTrace();
            }

        }
    }

    public static void loadConfig() {
        SimpleLog.LEVEL = config.getEnum("logging_level", SimpleLog.Level.INFO, "Level of logging.");

        Config.SERVER_DIR = config.getString("serverdir", "server", "what is the name of the server-directory");

        String region = config.getString("region", "EUW", "The Region to use for chat + api");
        Config.CHAT_REGION = ChatServer.valueOf(region.toUpperCase());
        Config.REGION = Region.valueOf(region.toUpperCase());

        Config.API_KEY = config.getString("API_KEY", "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx", "Your api-key goes here. get it at developer.riotgames.com");
        Config.RATE10SECS = config.getInteger("rate10secs", 10, "riot api rate limit for 10 seconds, 0 disables rate-limiting");
        Config.RATE10MINS = config.getInteger("rate10mins", 500, "riot api rate limit for 10 minutes, 0 disables rate-limiting");

        Config.USERNAME = config.getString("username", "", "Chat username (your lol login name)");
        Config.PASSWORD = config.getString("password", "", "Chat password (your lol login password)");
        Config.ACCEPT_FRIENDS = config.getBoolean("acceptFriends", false, "Should friend-requests be accepted automatically?");

        Config.STATUS_MSG = config.getString("status_message", "Nothing suspicious here...", "The status-message displayed to others in the friendslist");
        Config.STATUS_LEVEL = config.getInteger("status_level", 9001, "Level to display in friendslist");
        Config.STATUS_CHATMODE = config.getEnum("status_chatmode", ChatMode.BUSY, null);
        Config.STATUS_QUEUE = config.getEnum("status_queue", LolStatus.Queue.RANKED_SOLO_5x5, "Your current queue status.");
        Config.STATUS_TIER = config.getEnum("status_tier", LolStatus.Tier.CHALLENGER, null);
        Config.STATUS_DIVISION = config.getEnum("status_division", LolStatus.Division.NONE, null);
        Config.STATUS_RANKEDWINS = config.getInteger("status_rankedwins", 999999, "Ranked wins to display");
        Config.STATUS_NORMALWINS = config.getInteger("status_normalwins", 0, "Normal wins to display");

        saveConfig();
    }

    private static void handleVersionChange(int oldVersion) {
        switch(oldVersion) {
            case 2:
                config.setEnum("status_chatmode", ChatMode.valueOf(config.getString("status_chatmode", "BUSY", null)), null);
                config.setEnum("status_queue", LolStatus.Queue.valueOf(config.getString("status_queue", "RANKED_SOLO_5x5", null)), null);
                config.setEnum("status_tier", LolStatus.Tier.valueOf(config.getString("status_tier", "CHALLENGER", null)), null);
                config.setEnum("status_division", LolStatus.Division.valueOf(config.getString("status_division", "NONE", null)), null);

                config.deleteString("status_chatmode");
                config.deleteString("status_queue");
                config.deleteString("status_tier");
                config.deleteString("status_division");
                break;
        }
    }

    public static void saveConfig() {
        if(config.hasChanged()) {
            try {
                config.save();
            } catch (ConfigurationException e) {
                log.warn("Error saving config file");
                e.printStackTrace();
            }
        }
    }
}
