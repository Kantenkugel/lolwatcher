package com.kantenkugel.lolchat.config;

import org.apache.commons.configuration.ConfigurationException;

/**
 * Created by Kantenkugel.
 */
public interface IConfiguration {
    public <T extends Enum<T>> T getEnum(String key, T defaultValue, String comment);
    public <T extends Enum<T>> void setEnum(String key, T value, String comment);
    public <T extends Enum<T>> void deleteEnum(String key);

    public boolean getBoolean(String key, boolean defaultValue, String comment);
    public void setBoolean(String key, boolean value, String comment);
    public void deleteBoolean(String key);

    public int getInteger(String key, int defaultValue, String comment);
    public void setInteger(String key, int value, String comment);
    public void deleteInteger(String key);

    public String getString(String key, String defaultValue, String comment);
    public void setString(String key, String value, String comment);
    public void deleteString(String key);

    public boolean hasChanged();

    public boolean isNew();

    public boolean hasVersionChanged();
    public int getOldVersion();

    public void save() throws ConfigurationException;

}
