package com.kantenkugel.lolchat.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kantenkugel.lolchat.statics.Config;
import com.kantenkugel.lolchat.statics.StaticRiotData;
import com.kantenkugel.lolchat.statics.SummonerHelper;
import com.kantenkugel.lolchat.util.FileHelper;

import java.io.File;

/**
 * Created by Kantenkugel.
 */
public class ServerController {
    private static FileHelper serverFileHelper;
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    public static void init() {
        if(serverFileHelper == null) {
            serverFileHelper = new FileHelper(Config.SERVER_DIR);
        }
    }

    public static void updateStatics() {
        serverFileHelper.saveFile("champions.txt", GSON.toJson(StaticRiotData.champions));
        serverFileHelper.saveFile("spells.txt", GSON.toJson(StaticRiotData.spells));
        serverFileHelper.saveFile("items.txt", GSON.toJson(StaticRiotData.items));
    }

    public static void updateSummoners() {
        serverFileHelper.saveFile("summoners.txt", GSON.toJson(SummonerHelper.getCopy()));
    }

    public static File getDirectory() {
        return serverFileHelper.getParent();
    }
}
