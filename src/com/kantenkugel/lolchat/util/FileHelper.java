package com.kantenkugel.lolchat.util;

import com.kantenkugel.lolchat.ChatMain;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

/**
 * Created by Kantenkugel.
 */
public class FileHelper {
    private static FileHelper mother;
    private File parent;

    public FileHelper(String targetDir) {
        this(new File(mother==null?null:mother.getParent(), targetDir));
    }

    public FileHelper(File parent, String targetDir) {
        this(new File(parent, targetDir));
    }

    public FileHelper(File parentFile) {
        parent = parentFile;
        while(parent.exists() && !parent.isDirectory()) {
            parent = new File(parent.getParentFile(), parent.getName() + "new");
        }
        if(!parent.exists()) {
            if(!parent.mkdirs()) {
                ChatMain.MAINLOG.fatal("Error creating directory structure for folder " + parent.getAbsolutePath());
            }
        }
        if(mother == null) {
            mother = this;
        }
    }

    public boolean saveFile(String name, String content) {
        try {
            Files.write(new File(parent, name).toPath(), content.getBytes(StandardCharsets.UTF_8), StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean existsIsFile(String name) {
        File theFile = new File(parent, name);
        return theFile.exists() && !theFile.isDirectory();
    }

    public boolean exists(String name) {
        File theFile = new File(parent, name);
        return theFile.exists();
    }

    public String readIfExists(String name) {
        File file = new File(parent, name);
        if(!file.exists() || file.isDirectory()) {
            return null;
        }
        try {
            return new String(Files.readAllBytes(file.toPath()), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static FileHelper getMother() {
        return mother;
    }

    public File getParent() {
        return parent;
    }
}
