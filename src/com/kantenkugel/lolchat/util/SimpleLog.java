package com.kantenkugel.lolchat.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Kantenkugel.
 */
public class SimpleLog {
    public static Level LEVEL = Level.INFO;

    private static final String FORMAT = "[%time%] [%level%] [%name%]: %text%";
    private static final SimpleDateFormat DFORMAT = new SimpleDateFormat("HH:mm:ss");

    private static final Map<String, SimpleLog> LOGS = new HashMap<>();

    public static SimpleLog getLog(String name) {
        synchronized (LOGS) {
            if(!LOGS.containsKey(name.toLowerCase())) {
                LOGS.put(name.toLowerCase(), new SimpleLog(name));
            }
        }
        return LOGS.get(name.toLowerCase());
    }

    private final String name;
    private Level level = null;

    private SimpleLog(String name) {
        this.name = name;
    }

    public void setLevel(Level lev) {
        this.level = lev;
    }

    public void log(Level level, Object msg) {
        if(level.getPriority() < ((this.level == null) ? SimpleLog.LEVEL.getPriority() : this.level.getPriority())) {
            return;
        }
        if(level == Level.OFF) {
            return;
        }
        String out = FORMAT.replace("%time%", DFORMAT.format(new Date())).replace("%level%", level.getTag()).replace("%name%", name).replace("%text%", String.valueOf(msg));
        if(level.isError) {
            System.err.println(out);
        } else {
            System.out.println(out);
        }
    }

    public void trace(Object msg) {
        log(Level.TRACE, msg);
    }

    public void debug(Object msg) {
        log(Level.DEBUG, msg);
    }

    public void info(Object msg) {
        log(Level.INFO, msg);
    }

    public void warn(Object msg) {
        log(Level.WARNING, msg);
    }

    public void fatal(Object msg) {
        log(Level.FATAL, msg);
    }


    public static enum Level {
        ALL("Finest", 0, false),
        TRACE("Trace", 1, false),
        DEBUG("Debug", 2, false),
        INFO("Info", 3, false),
        WARNING("Warning", 4, true),
        FATAL("Fatal", 5, true),
        OFF("NO-LOGGING", 6, true);

        private String msg;
        private int pri;
        private boolean isError;

        private Level(String message, int priority, boolean isError) {
            this.msg = message;
            this.pri = priority;
            this.isError = isError;
        }

        public String getTag() {
            return msg;
        }

        public int getPriority() {
            return pri;
        }

        public boolean isError() {
            return isError;
        }
    }
}
