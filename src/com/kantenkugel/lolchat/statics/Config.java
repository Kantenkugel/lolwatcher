package com.kantenkugel.lolchat.statics;

import com.github.theholywaffle.lolchatapi.ChatMode;
import com.github.theholywaffle.lolchatapi.ChatServer;
import com.github.theholywaffle.lolchatapi.LolStatus;
import lol4j.util.Region;


public class Config {
    //Global
    public static final String CONFIG_PATH = "config.properties";
    public static final String CONFIG_COMMENT = "LoLBot configuration file";
    public static final int GAMEVERSION = 1;
    public static final int CONFIGVERSION = 3;

    public static final int CLEANER_SLEEP = 60 * 10;

    //Set by config-file
    public static String SERVER_DIR = "server";

    public static String API_KEY = "7edf06a7-1a26-4ac6-a180-305d6912a5c6";
    public static int RATE10SECS = 10;
    public static int RATE10MINS = 500;

    public static Region REGION = Region.EUW;

    public static ChatServer CHAT_REGION = ChatServer.EUW;

    public static String USERNAME = "Kantenkugel2";
    public static String PASSWORD = "ritterM35";

    public static boolean ACCEPT_FRIENDS = true;

    public static String STATUS_MSG = "Nothing suspicious here";
    public static int STATUS_LEVEL = 9001;
    public static LolStatus.Queue STATUS_QUEUE = LolStatus.Queue.RANKED_SOLO_5x5;
    public static LolStatus.Tier STATUS_TIER = LolStatus.Tier.CHALLENGER;
    public static LolStatus.Division STATUS_DIVISION = LolStatus.Division.I;
    public static int STATUS_RANKEDWINS = 999999;
    public static int STATUS_NORMALWINS = 0;
    public static ChatMode STATUS_CHATMODE = ChatMode.BUSY;
}
