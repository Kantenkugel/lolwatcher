package com.kantenkugel.lolchat.statics;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.kantenkugel.lolchat.server.ServerController;
import lol4j.client.impl.Lol4JClientImpl;
import lol4j.protocol.dto.summoner.SummonerDto;

import java.net.URLEncoder;
import java.util.Map;

/**
 * Created by Kantenkugel.
 */
public class SummonerHelper {
    private static Lol4JClientImpl riot;

    public static void setApi(Lol4JClientImpl api) {
        riot = api;
    }

    public static String getName(long id) {
        if(StaticRiotData.summoners.containsKey(id)) {
            return StaticRiotData.summoners.get(id);
        }
        String summonerName = riot.getSummonerName(id, Config.REGION);
        StaticRiotData.summoners.put(id, summonerName.toLowerCase());
        ServerController.updateSummoners();

        return summonerName;
    }

    public static long getId(String name) {
        if(StaticRiotData.summoners.containsValue(name.toLowerCase())) {
            return StaticRiotData.summoners.inverse().get(name.toLowerCase());
        }
        try {
            SummonerDto summ = riot.getSummonerByName(URLEncoder.encode(name, "UTF-8"), Config.REGION);
            if(summ != null) {
                StaticRiotData.summoners.put(summ.getId(), name.toLowerCase());
                ServerController.updateSummoners();
                return summ.getId();
            }
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    public static int size() {
        return StaticRiotData.summoners.size();
    }

    public static void fillWith(Map<? extends Long, ? extends String> filler) {
        StaticRiotData.summoners = HashBiMap.create(filler);
    }

    public static BiMap<Long, String> getCopy() {
        return HashBiMap.create(StaticRiotData.summoners);
    }
}
