package com.kantenkugel.lolchat.statics;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import lol4j.protocol.dto.lolstaticdata.BasicDataDto;
import lol4j.protocol.dto.lolstaticdata.ChampionDto;
import lol4j.protocol.dto.lolstaticdata.SummonerSpellDto;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Kantenkugel.
 */
public class StaticRiotData {
    public static Map<Integer, ChampionDto> champions = new HashMap<>();

    public static Map<Integer, SummonerSpellDto> spells = new HashMap<>();

    public static Map<Integer, BasicDataDto> items = new HashMap<>();

    static BiMap<Long, String> summoners = HashBiMap.create();
}
