package com.kantenkugel.lolchat;

import com.github.theholywaffle.lolchatapi.LolChat;
import com.github.theholywaffle.lolchatapi.LolStatus;
import com.github.theholywaffle.lolchatapi.listeners.ChatListener;
import com.github.theholywaffle.lolchatapi.listeners.FriendListener;
import com.github.theholywaffle.lolchatapi.wrapper.Friend;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kantenkugel.lolchat.config.ConfigHandler;
import com.kantenkugel.lolchat.game.GameController;
import com.kantenkugel.lolchat.server.ServerController;
import com.kantenkugel.lolchat.statics.Config;
import com.kantenkugel.lolchat.statics.StaticRiotData;
import com.kantenkugel.lolchat.statics.SummonerHelper;
import com.kantenkugel.lolchat.util.FileHelper;
import com.kantenkugel.lolchat.util.SimpleLog;
import lol4j.client.impl.Lol4JClientImpl;
import lol4j.protocol.dto.league.LeagueDto;
import lol4j.protocol.dto.league.LeagueEntryDto;
import lol4j.protocol.dto.lolstaticdata.BasicDataDto;
import lol4j.protocol.dto.lolstaticdata.ChampionDto;
import lol4j.protocol.dto.lolstaticdata.SummonerSpellDto;
import lol4j.protocol.dto.summoner.SummonerDto;
import lol4j.util.Region;
import lol4j.util.league.Queue;
import lol4j.util.lolstaticdata.ChampData;
import lol4j.util.lolstaticdata.ItemData;
import lol4j.util.lolstaticdata.SummonerSpellData;

import javax.ws.rs.NotFoundException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.LogManager;


public class ChatMain {
	private static final String VERSION = "0.1.0";

    public static final SimpleLog MAINLOG = SimpleLog.getLog("Main");
    private static final SimpleLog CHATLOG = SimpleLog.getLog("Chat");

    private final Region REGION = Config.REGION;

    private final Lol4JClientImpl riot;
    private final LolChat chat;

    private final GameController gController;

    public static void main(String[] args) {
        SimpleLog.getLog("Game").setLevel(SimpleLog.Level.DEBUG);

        ConfigHandler.init();

        ServerController.init();

        LogManager.getLogManager().reset(); // Disable logging of Smack
        ChatMain main = new ChatMain();

        main.initStaticData();

        if(main.connect()) {
            main.setChatStatus();
            main.addListeners();
        }
    }

    private void initStaticData() {
        StaticRiotData.champions = new HashMap<>();
        List<ChampData> requestedChamp = new ArrayList<>();
        requestedChamp.add(ChampData.IMAGE);
        for(ChampionDto championDto : riot.getChampionList(REGION, null, null, requestedChamp).getData().values()) {
            StaticRiotData.champions.put(Integer.parseInt(championDto.getId()), championDto);
        }

        StaticRiotData.spells = new HashMap<>();
        List<SummonerSpellData> requestedSpell = new ArrayList<>();
        requestedSpell.add(SummonerSpellData.IMAGE);
        for(SummonerSpellDto spellDto : riot.getSummonerSpellList(REGION, null, null, requestedSpell).getData().values()) {
            StaticRiotData.spells.put(spellDto.getId(), spellDto);
        }

        StaticRiotData.items = new HashMap<>();
        List<ItemData> requestedItem = new ArrayList<>();
        requestedItem.add(ItemData.IMAGE);
        for(BasicDataDto itemData : riot.getItemList(REGION, null, null, requestedItem).getData().values()) {
            StaticRiotData.items.put(itemData.getId(), itemData);
        }


        //Load already cached summoner ids + names
        FileHelper helper = FileHelper.getMother();
        String contents = helper.readIfExists("summoners.txt");
        if(contents != null) {
            Gson gson = new Gson();
            Type typeOfHashMap = new TypeToken<Map<Long, String>>() {}.getType();
            Map<Long, String> tmpmap = gson.fromJson(contents, typeOfHashMap);
            SummonerHelper.fillWith(tmpmap);
        }
        MAINLOG.info("Read " + SummonerHelper.size() + " summoners from cached file");

        ServerController.updateStatics();
    }

    public ChatMain() {
        ConfigHandler.init();

        riot = new Lol4JClientImpl(Config.API_KEY);
        if(Config.RATE10MINS > 0 || Config.RATE10SECS > 0) {
            riot.setRateLimit(Config.RATE10SECS, Config.RATE10MINS);
        }
        SummonerHelper.setApi(riot);

        chat = new LolChat(Config.CHAT_REGION, Config.ACCEPT_FRIENDS);

        ServerController.init();
        gController = new GameController(riot, ServerController.getDirectory());
    }

    public boolean connect() {
        if(chat.login(Config.USERNAME, Config.PASSWORD)) {
            //Wait to recieve initial data
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            CHATLOG.info("Done logging in!");

            CHATLOG.info("Currently online friends are:");
            StringBuilder b = new StringBuilder();
            for(Friend f : chat.getFriends()) {
                checkName(f);
                if(f.isOnline()) {
                    b.append(f.getName()).append("(").append(f.getUserId()).append("), ");
                }
            }
            b.setLength(b.length()-2);
            CHATLOG.info(b.toString());
            return true;
        } else {
            CHATLOG.fatal("Error logging in!");
            return false;
        }
    }

    public void addListeners() {
        chat.addChatListener(new ChatListener() {
            public void onMessage(Friend friend, String message) {
                CHATLOG.info(friend.getName() + ": " + message);
                String[] parts = message.split(" ");
                if(parts[0].equals("lookup") && parts.length >= 2) {
                    SummonerDto summ = riot.getSummonerByName(message.substring(7).replace(" ", ""), Region.EUW);
                    try {
                        List<LeagueDto> leagues = riot.getLeaguesData(summ.getId(), Region.EUW);
                        boolean found = false;
                        for(LeagueDto league : leagues) {
                            if(league.getQueue() == Queue.RANKED_SOLO_5X5) {
                                for(LeagueEntryDto entry : league.getEntries()) {
                                    if(entry.getPlayerOrTeamName().equalsIgnoreCase(summ.getName())) {
                                        friend.sendMessage(summ.getName() + " is currently in " + league.getTier() + " " + entry.getDivision()
                                                + " with " + entry.getLeaguePoints() + " LP and " + entry.getWins() + " overall wins");
                                        break;
                                    }
                                }
                                found = true;
                                break;
                            }
                        }
                        if(!found)
                            friend.sendMessage(summ.getName() + " is currently not ranked in Solo-Queue");
                    } catch (NotFoundException ex) {
                        friend.sendMessage(message.substring(7) + " is currently not ranked in  any Queue");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else if(parts[0].equals("help")) {
                    friend.sendMessage("Available Commands are:");
                    friend.sendMessage("help - prints this help");
                    friend.sendMessage("lookup name - prints stats for given user");
                    friend.sendMessage("about - prints informations about this bot");
                } else if(parts[0].equals("about")) {
                    friend.sendMessage("ChatBot by Kantenkugel");
                    friend.sendMessage("Version " + VERSION);
                    friend.sendMessage("This product is not endorsed, certified or otherwise approved in any way" +
                            " by Riot Games, Inc. or any of its affiliates.");
                } else {
                    friend.sendMessage("Command could not be interpretted. Try \"help\" for help");
                }
            }
        });

        chat.addFriendListener(new FriendListener() {
            @Override
            public void onFriendStatusChange(Friend friend) {
                CHATLOG.debug("~~" + friend.getName() + ": " + friend.getStatus().getGameStatus());
                gController.statusUpdate(friend.getName(), friend.getStatus().getGameStatus());
            }

            @Override
            public void onFriendLeave(Friend friend) {
                CHATLOG.trace("--" + friend.getName() + " (" + friend.getUserId() + ") logged out");
            }

            @Override
            public void onFriendJoin(Friend friend) {
                checkName(friend);
                CHATLOG.trace("++" + friend.getName() + " (" + friend.getUserId() + ") logged in");
            }

            @Override
            public void onFriendBusy(Friend friend) {
            }

            @Override
            public void onFriendAway(Friend friend) {
            }

            @Override
            public void onFriendAvailable(Friend friend) {
            }
        });
    }

    public void setChatStatus() {
        LolStatus newStatus = new LolStatus();
        newStatus.setStatusMessage(Config.STATUS_MSG);
        newStatus.setLevel(Config.STATUS_LEVEL);
        newStatus.setRankedLeagueQueue(Config.STATUS_QUEUE);
        newStatus.setRankedLeagueTier(Config.STATUS_TIER);
        newStatus.setRankedLeagueDivision(Config.STATUS_DIVISION);
        newStatus.setRankedWins(Config.STATUS_RANKEDWINS);
        newStatus.setNormalWins(Config.STATUS_NORMALWINS);
        chat.setStatus(newStatus);

        chat.setChatMode(Config.STATUS_CHATMODE);
    }
	
	private void checkName(Friend f) {
		if(f.getName() == null) {
			try {
                String username = SummonerHelper.getName(Long.parseLong(f.getUserId().substring(3, f.getUserId().length() - 8)));
                if(username != null)
                    f.setName(username);
            } catch (NumberFormatException ex) {
                CHATLOG.warn("Somehow we didn't get a numer :(");
			}
		}
	}
}
