# Simple LoL match-watcher and match-summary Generator #

## Description ##

This project is still under heavy development and is not yet working correctly!
At the end, this bot should watch in the LoL ingame chat and start creating json-formatted match-overviews
with stats at the end of a users game (like riot does with its new match-history, just not as detailed).

### Bugs ###

* Can't find game in match-history of a lot of summoners (_Still looking at why this happens_)

### Missing ###

* PHP-Script to generate a visual representation of the saved data

## Dependencies ##

* gson-2.2.4
* commons-configuration-1.10
    * commons-lang-2.6
    * commons-logging-1.1.1
* ___lol4j by aaryn101___
    * ___commons-lang3-3.2.2___
    * ___jackson-core-2.3.0___
    * ___jackson-databind-2.3.0___
    * ___jackson-annotations-2.3.0___
    * ___jersey-client-2.4.1___
* ___lolchatapi by TheHolyWaffle___
    * ___jdom-2.0.5___
    * ___smack-3.4.0___
    * ___smackx-2.4.0___

___Fat-Cursive___ entries are already included as src or as libs

## License ##

The Whole project is licensed under the MIT license. See the LICENSE file for further informations

## Legalese ##

This product is not endorsed, certified or otherwise approved in any way by Riot Games, Inc. or any of its affiliates.
League of Legends and Riot Games are trademarks or registered trademarks of Riot Games, Inc. League of Legends © Riot Games, Inc